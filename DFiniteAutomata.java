/*
 * c1639550 - Theory of Computation Coursework
 */

package dfiniteautomata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DFiniteAutomata
{
    public static void main(String[] args) 
    {
        // MENU SYSTEM.
        Scanner choice = new Scanner(System.in);
        System.out.println("Please select which task you would like to run (1,2,3,4,5):");
        switch(choice.nextInt()) {
            case 1:
                System.out.println("Input the filename without the extension eg. .txt)");
                System.out.println("enter blank line when you wish to quit.");
                Scanner in = new Scanner(System.in);
                ArrayList<String> files = new ArrayList<>();

                // Can have multiple files.
                while(true)
                {
                    String response = in.nextLine();
                    if (response.isEmpty())
                    {
                        break;
                    }
                    files.add(response);
                }

                // For every file run task one.
                files.forEach((file) -> {
                    TaskOne(file + ".txt");
                    System.out.println("");
                });
                break;
                
            case 2:
                System.out.println("Input the 2 file names without the extension eg. .txt)");
                Scanner int2 = new Scanner(System.in);
                String[] filest2 = new String[2];

                for (int i = 0; i < 2; i++)
                {
                    String response = int2.nextLine();
                    filest2[i] = (response + ".txt");
                }
                DFA insertDfa1 = new DFA(filest2[0]);
                DFA insertDfa2 = new DFA(filest2[1]);
                System.out.println("");
                PrintEncoding(Insertion(insertDfa1, insertDfa2));
                break;
            case 3: 
                System.out.println("Input the 2 file names without the extension eg. .txt)");
                Scanner int3 = new Scanner(System.in);
                String[] filest3 = new String[2];

                for (int i = 0; i < 2; i++)
                {
                    String response = int3.nextLine();
                    filest3[i] = (response + ".txt");
                }

                PrintEncoding(TaskThree(filest3));
                break;
            case 4:
                System.out.println("Input filename without the extension eg. .txt)");
                Scanner int4 = new Scanner(System.in);
                DFA dfsDfa1 = new DFA(int4.next()+".txt");
                
                ArrayList<ArrayList<String>> pathAndLang = TaskFour(dfsDfa1);
		if(pathAndLang.get(0).isEmpty())
                {
                    System.out.println("language empty");
                }
                else 
                {
                    if (pathAndLang.get(1).isEmpty())
                    {
                        System.out.println("Init state is also end state.");
                    }
                    else
                    {
                        System.out.println("Language non-empty - " + 
                            pathAndLang.get(1).toString() + " accepted");
                    }
                }
                break;
            case 5: 
                System.out.println("Input the 2 file names without the extension eg. .txt)");
                Scanner int5 = new Scanner(System.in);
                String[] filest5 = new String[2];

                for (int i = 0; i < 2; i++)
                {
                    String response = int5.nextLine();
                    filest5[i] = (response + ".txt");
                }

                if(TaskFive(filest5))
                {
                    System.out.println("equivalent");
                }
                else
                {
                    System.out.println("not equivalent");
                }
                break;
            default:
                System.out.println("Choice not valid!!! Please try again later.");
                break;
        }
    }   

    public static void TaskOne(String filename)
    { 
        DFA normalDfa = new DFA(filename);
        // print "before" encoding.
        System.out.println("Normal");
        PrintEncoding(normalDfa);
        System.out.println("");
        
        // Print valid moves/encoding using Conj/complement function.
        System.out.println("Conjugate");
        PrintEncoding(Conj(filename));
    }
    
    public static DFA Conj(String filename)
    {
        DFA a = new DFA(filename);
        DFA b = new DFA(filename);
        
        // Create copy of a and swap final states/states.
        ArrayList<String> conjD = a.getStates();
        conjD.removeAll(a.getFinalStates());
        b.setFinalStates(conjD);
        
        return b;
    }
    
    public static void PrintEncoding(DFA d)
    {
        // Regex removes both brackets and the comma gained from turn to arr.
        System.out.println(d.getStates().size());
        // Regex to remove the brackets and comma from changing array to string
        System.out.println(Arrays.toString(d.getStates().toArray())
                .replaceAll("\\[|]|,",""));
        System.out.println(d.getAlphabet().size());
        System.out.println(Arrays.toString(d.getAlphabet().toArray())
                .replaceAll("\\[|]|,",""));
        // Use loop to print out each arraylist of arraylist as string.
        d.getTransitions().forEach((t) -> {
            System.out.println(Arrays.toString(t.toArray())
                    .replaceAll("\\[|]|,","")); });
        System.out.println(d.getStartState());
        System.out.println(d.getFinalStates().size());
        System.out.println(Arrays.toString(d.getFinalStates().toArray())
                .replaceAll("\\[|]|,",""));
    }
    
    // Union/Symmetric Diff
    public static DFA TaskThree(String[] files)
    {
        // To create the symm diff we need both conj and norm copies of dfas.
        DFA d1conj = Conj(files[0]);
        DFA d2conj = Conj(files[1]);
        DFA d1norm = new DFA(files[0]);
        DFA d2norm = new DFA(files[1]);
        
        // Perform intersection on inversed dfas.
        DFA dIntersct1 = Insertion(d1conj, d2norm);
        DFA dIntersct2 = Insertion(d1norm, d2conj);
        
        // get final states by looping over and adding both final states to single var.
        ArrayList<String> finalStates = new ArrayList<>();
        dIntersct1.getFinalStates().forEach((d1f) -> {
            if (!finalStates.contains(d1f))
            {
                // add final states.
                finalStates.add(d1f);    
            }});
        dIntersct2.getFinalStates().forEach((d2f) -> {
            if (!finalStates.contains(d2f))
            {
                // add final states.
                finalStates.add(d2f);    
            }});
        
        // use dintersect1 as final DFA to return.
        dIntersct1.setFinalStates(finalStates);
        
        return dIntersct1;
    }
    
    // Intersection.
    public static DFA Insertion(DFA d1, DFA d2)
    {
        // add together start states of both dfas.
        String startState = d1.getStartState()+d2.getStartState();
        
        // create var to hold the new states formed that will be used for transitions.
        ArrayList<String> newStates = new ArrayList<>();
        
        // Concat both dfa1 and dfa2s states to new states.
        d1.getStates().forEach((s) -> {
            d2.getStates().forEach((s2) -> {
                newStates.add(s.concat(s2));
            });
        });
        
        // Loop States, split single state, work out a and b new state
        ArrayList<ArrayList<String>> parts = new ArrayList<>();
        
        for (int i = 0; i < newStates.size(); i++) 
        {
            // get location of positions.
            String s1 = newStates.get(i).substring(0,(newStates.get(i).length()/2));
            String s2 = newStates.get(i).substring(newStates.get(i).length()/2);

            // Get old position to work out new one.
            int i1 = d1.getStates().indexOf(s1);
            int i2 = d2.getStates().indexOf(s2);
            parts.add(new ArrayList<>());
            for (int x = 0; x < d1.getAlphabet().size(); x++)
            {
                // Work out new pos and add loc to parts var.
                String t1 = d1.getTransitions().get(i1).get(x);
                String t2 = d2.getTransitions().get(i2).get(x);
                parts.get(i).add(t1+t2);
            }
        }
        
        // Work out final states
        ArrayList<String> finalStates = new ArrayList<>();
        d1.getFinalStates().forEach((d1f) -> {
            d2.getFinalStates().forEach((d2f) -> {
                finalStates.add(d1f+d2f);
            });
        });

        // Create final DFA using obj and return that obj.
        DFA finalIDFA = new DFA();
        finalIDFA.setAlphabet(d1.getAlphabet());
        finalIDFA.setStartState(startState);
        finalIDFA.setFinalStates(finalStates);
        finalIDFA.setStates(newStates);
        finalIDFA.setTransitions(parts);
        return finalIDFA;
    }
    
    // Non-Emptyness
    // Used this for ref - java2blog.com/depth-first-search-in-java
    public static ArrayList<ArrayList<String>> TaskFour(DFA m)
    {
        ArrayList<ArrayList<String>> pathAndLang = new ArrayList<>();
        // instanc twice as we want to add one for paths and one for visited transacts.
        // 0 == path and 1 == lang. 3rd is for if start state is also end state.
        pathAndLang.add(new ArrayList<>());
        pathAndLang.add(new ArrayList<>());
        pathAndLang.get(0).add(m.getStartState());
        
        // Check if start state is final state.
        if(m.getFinalStates().contains(m.getStartState())) 
        {
            return pathAndLang;
        }
        else
        {
            // Rerun recursive func.
            pathAndLang = DFS(m, m.getStartState(), pathAndLang);
            
            // if the last value is not equal any in the final states then -
            if (!m.getFinalStates().contains(pathAndLang.get(0)
                    .get(pathAndLang.get(0).size()-1)))
            { 
                // Remove all paths and languages.
                pathAndLang.get(1).removeAll(pathAndLang.get(1));
                pathAndLang.get(0).removeAll(pathAndLang.get(0));                    
            }
            return pathAndLang;
        }
    }
    
    // Depth First Search
    public static ArrayList<ArrayList<String>> DFS(DFA m, String state, 
            ArrayList<ArrayList<String>> pathAndLang)
    {
        // Loop over the alphabet to run through all paths.
        for (int i = 0; i < m.getAlphabet().size(); i++)
        {
            // Get the index where the state is in m states.
            int statePlace = m.getStates().indexOf(state);
            state = m.getTransitions().get(statePlace).get(i);
            
            // If our visited states does not contain our current one then...
            if (!pathAndLang.get(0).contains(state))
            {
                // Add state.
                pathAndLang.get(0).add(state);
                pathAndLang.get(1).add(m.getAlphabet().get(i));
                
                // If is final state, return.
                if (m.getFinalStates().contains(state))
                {
                    return pathAndLang;
                }
                else
                {
                    DFS(m, state, pathAndLang);
                    // Before return, if first state in paths is == to any final states
                    // then return.
                    if (m.getFinalStates().contains(pathAndLang.get(0).get(0)))
                    {
                        return pathAndLang;
                    }
                }
            }
        }
        return pathAndLang;
    }
    
    // Check if equiv.
    public static boolean TaskFive(String[] files)
    {
        // if union diff has language then they are not equiv - accept same lang?
        return (TaskFour(TaskThree(files)).get(0).isEmpty());
    }
}
