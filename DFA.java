package dfiniteautomata;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DFA 
{
    // DFA vars privately accessible by getters & setters.
    private ArrayList<String> states = new ArrayList<>();
    private ArrayList<String> alphabet = new ArrayList<>();
    private ArrayList<ArrayList<String>> transitions = new ArrayList<>();
    private String startState;
    private ArrayList<String> finalStates = new ArrayList<>();
    
    // Blank object in case need to create temp obj.
    public DFA() { }
    
    public DFA(String filename)
    {
        Scanner in = null;
        List<String> d = new ArrayList<>();

        // Check if DFA file exists.
        try {
            in = new Scanner(new FileReader(filename));
        } catch (FileNotFoundException ex){System.err.println("File not found."); System.exit(-1);}
        
        while(in.hasNextLine()) 
        {
            d.add(in.nextLine());
        }
        in.close();
        
        // split by space using line refs from proforma.
        states = new ArrayList<>(Arrays.asList(d.get(1).split(" ")));
        alphabet = new ArrayList<>(Arrays.asList(d.get(3).split(" ")));
        
        for(int i = 0; i < states.size(); i++)
        {
            // add transtitions using file line, using lines for ref. split by space.
            transitions.add(new ArrayList<>(Arrays.asList(d.get(i+4).split(" "))));
        }
        // Will work with diff num of states. 4 because 4 lines before transitions are used.
        startState = d.get(4 + states.size());
        
        // Create final states array list, 6 is point in list that final states...
        // are, use this to get past the transitions and num of accept states.
        finalStates = new ArrayList<>(Arrays.asList(d.get(6 + states.size()).split(" ")));
    }

    // Getters & Setters
    
    public ArrayList<String> getStates() {
        return states;
    }

    public ArrayList<String> getAlphabet() {
        return alphabet;
    }

    public ArrayList<ArrayList<String>> getTransitions() {
        return transitions;
    }

    public String getStartState() {
        return startState;
    }

    public ArrayList<String> getFinalStates() {
        return finalStates;
    }

    public void setStates(ArrayList<String> states) {
        this.states = states;
    }

    public void setAlphabet(ArrayList<String> alphabet) {
        this.alphabet = alphabet;
    }

    public void setTransitions(ArrayList<ArrayList<String>> transitions) {
        this.transitions = transitions;
    }

    public void setStartState(String startState) {
        this.startState = startState;
    }

    public void setFinalStates(ArrayList<String> finalStates) {
        this.finalStates = finalStates;
    }
     
}
